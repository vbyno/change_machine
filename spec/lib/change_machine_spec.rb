# frozen_string_literal: true

describe ChangeMachine do
  describe '.call' do
    subject(:change_machine) { described_class.new(config) }

    context 'simple cases' do
      let(:config) do
        {
          50 => 2,
          25 => 8,
          10 => 14,
          5 => 11,
          2 => 3,
          1 => 1
        }
      end

      it 'gives change for multiple times' do
        expect(change_machine.call(2)).to eq [50, 50, 25, 25, 25, 25]

        expect(change_machine.call(2))
          .to eq [25, 25, 25, 25, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10]

        expect(change_machine.call(1))
          .to eq [10, 10, 10, 10, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 2, 2, 1]

        expect { change_machine.call(2) }
          .to raise_error(described_class::InsufficientFundsError)
      end
    end

    context 'full amount of the first coin pack has to be skipped' do
      let(:config) do
        {
          25 => 3,
          10 => 10
        }
      end

      it 'gives a correct change' do
        expect(change_machine.call(1)).to eq [25, 25, 10, 10, 10, 10, 10]

        expect { change_machine.call(1) }
          .to raise_error(described_class::InsufficientFundsError)
      end
    end

    context 'full amount of the second coin pack has to be skipped' do
      let(:config) do
        {
          50 => 1,
          25 => 1,
          10 => 15
        }
      end

      it 'gives a correct change' do
        expect(change_machine.call(1)).to eq [50, 10, 10, 10, 10, 10]
        expect(change_machine.call(1)).to eq [10] * 10

        expect { change_machine.call(1) }
          .to raise_error(described_class::InsufficientFundsError)
      end
    end

    context 'config has zero values' do
      let(:config) do
        {
          50 => 1,
          25 => 0,
          10 => 15
        }
      end

      it 'skips them' do
        expect(change_machine.call(1)).to eq [50, 10, 10, 10, 10, 10]
        expect(change_machine.call(1)).to eq [10] * 10

        expect { change_machine.call(1) }
          .to raise_error(described_class::InsufficientFundsError)
      end
    end

    context 'config is empty' do
      let(:config) { {} }

      it 'returns InsufficientFundsError' do
        expect { change_machine.call(1) }
          .to raise_error(described_class::InsufficientFundsError)
      end
    end

    context 'unhappy cases' do
      let(:config) { {} }

      it 'works only with fixnums' do
        expect { change_machine.call(1.0) }
          .to raise_error ArgumentError, 'Amount must be Fixnum'
      end

      it 'works only with positive numbers' do
        expect { change_machine.call(-1) }
          .to raise_error ArgumentError, 'Amount must be positive'

        expect { change_machine.call(0) }
          .to raise_error ArgumentError, 'Amount must be positive'
      end
    end
  end
end
