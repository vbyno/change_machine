# frozen_string_literal: true

describe ChangeMachine::Generator do
  describe '.call' do
    subject { described_class.call(money, 100) }

    context 'combination exists' do
      let(:money) { ChangeMachine::Money.build(50 => 1, 25 => 1, 10 => 50) }

      it 'generates a money object' do
        is_expected.to eq ChangeMachine::Money.build(50 => 1, 10 => 5)
      end
    end

    context 'no possible combination which satisfies the amount' do
      let(:money) { ChangeMachine::Money.build(50 => 1, 25 => 1, 10 => 4) }

      it 'returns nil' do
        is_expected.to be_nil
      end
    end
  end
end
