# frozen_string_literal: true

describe ChangeMachine::Money do
  describe '.build' do
    let(:hash) { { 50 => 1, 25 => 2 } }
    let(:expected_money) do
      described_class.new(
        [
          ChangeMachine::CoinSet.new(value: 50, number: 1),
          ChangeMachine::CoinSet.new(value: 25, number: 2)
        ]
      )
    end

    it 'builds a money object from hash' do
      expect(described_class.build(hash)).to eq expected_money
    end
  end

  describe '#-' do
    let(:money1) { described_class.build(50 => 3, 25 => 2) }
    let(:money2) { described_class.build(50 => 1, 25 => 2) }

    it 'subtracks' do
      expect(money1 - money2).to eq described_class.build(50 => 2, 25 => 0)
    end
  end

  describe '#sum' do
    let(:money) { described_class.build(50 => 3, 25 => 2) }

    it 'sums up all the coins' do
      expect(money.sum).to eq 200
    end
  end

  describe '#to_a' do
    let(:money) { described_class.build(50 => 3, 25 => 2) }

    it 'prints all coin values' do
      expect(money.to_a).to eq [50, 50, 50, 25, 25]
    end
  end
end
