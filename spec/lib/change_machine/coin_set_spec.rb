# frozen_string_literal: true

describe ChangeMachine::CoinSet do
  describe '#-' do
    let(:set1) { described_class.new(value: 50, number: 10) }
    let(:set2) { described_class.new(value: 50, number: 3) }
    let(:set3) { described_class.new(value: 25, number: 3) }
    let(:expected_coin_pack) { described_class.new(value: 50, number: 7) }

    it 'subtracks' do
      expect(set1 - set2).to eq expected_coin_pack
    end

    it 'does not subtrack if the second argument is bigger' do
      expect { set2 - set1 }
        .to raise_error ArgumentError, "Can't substract 10 from 3"
    end

    it 'does not subtrack if the denomination differs' do
      expect { set1 - set3 }.to raise_error(
        ArgumentError,
        "Can't substract coins of different denomination"
      )
    end

    it 'subtracks equal sets of coins' do
      expect((set1 - set1).number).to be_zero
    end
  end

  describe '#<=>' do
    let(:set1) { described_class.new(value: 25, number: 10) }
    let(:set2) { described_class.new(value: 50, number: 3) }
    let(:set3) { described_class.new(value: 25, number: 1) }

    specify { expect(set1 <=> set2).to eq 1 }
    specify { expect(set2 <=> set1).to eq(-1) }
    specify { expect(set1 <=> set3).to be_zero }
  end

  describe '#sum' do
    let(:set) { described_class.new(value: 25, number: 10) }

    specify { expect(set.sum).to eq 250 }
  end

  describe '#to_a' do
    context 'non empty pack' do
      let(:set) { described_class.new(value: 25, number: 3) }

      specify { expect(set.to_a).to eq [25, 25, 25] }
    end

    context 'empty pack' do
      let(:set) { described_class.new(value: 25, number: 0) }

      specify { expect(set.to_a).to eq [] }
    end
  end
end
