# frozen_string_literal: true

require 'rspec'
require 'pry'
require 'simplecov'

SimpleCov.start
require 'change_machine'
