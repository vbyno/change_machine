# frozen_string_literal: true

require_relative 'change_machine/coin_set'
require_relative 'change_machine/money'
require_relative 'change_machine/generator'

# A class to calculate the change for the amount of banknotes
class ChangeMachine
  InsufficientFundsError = Class.new(ArgumentError)

  BANKNOTE_TO_COINS = 100

  attr_reader :money

  def initialize(config)
    @money = Money.build(config)
  end

  def call(amount)
    raise ArgumentError, 'Amount must be Fixnum' unless amount.is_a?(Integer)
    raise ArgumentError, 'Amount must be positive' unless amount.positive?

    change = Generator.call(money, amount * BANKNOTE_TO_COINS)

    unless change
      raise InsufficientFundsError,
            "Impossible to find a change for #{amount} UAH"
    end

    self.money = money - change

    change.to_a
  end

  private

  attr_writer :money
end
