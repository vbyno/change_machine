# frozen_string_literal: true

class ChangeMachine
  # A set of unique coins sets
  class Money
    def self.build(hash)
      hash
        .map { |(value, number)| CoinSet.new(value: value, number: number) }
        .yield_self { |coin_sets| new(coin_sets) }
    end

    attr_reader :coin_sets, :sorted_coin_sets

    def initialize(coin_sets)
      @coin_sets = coin_sets.select { |set| set.number.positive? }.sort
    end

    def -(other)
      coin_sets
        .map { |coin_sets| coin_sets - other.take_pair(coin_sets) }
        .yield_self { |coin_sets| self.class.new(coin_sets) }
    end

    def sum
      coin_sets.inject(0) do |result, coin_sets|
        result + coin_sets.sum
      end
    end

    def to_a
      coin_sets.each_with_object([]) do |coin_sets, result|
        result.concat(coin_sets.to_a)
      end
    end

    def ==(other)
      coin_sets == other.coin_sets
    end

    protected

    def take_pair(coin_sets)
      coin_sets_hash.fetch(
        coin_sets.value,
        CoinSet.new(value: coin_sets.value, number: 0)
      )
    end

    def coin_sets_hash
      @coin_sets_hash ||= coin_sets.map { |set| [set.value, set] }.to_h
    end
  end
end
