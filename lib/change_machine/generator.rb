# frozen_string_literal: true

class ChangeMachine
  # Generates a change based on the existing money and requested amount
  class Generator
    def self.call(*args)
      new(*args).call
    end

    attr_reader :money, :limit

    def initialize(money, limit)
      @money = money
      @limit = limit
      @current_limit = limit
      @values = money.coin_sets.map(&:value)
      @numbers = Array.new(max_numbers.size, 0)
      @limits = Array.new(max_numbers.size, 0)
      @n = values.size - 1
    end

    def call
      position = 0

      loop do
        recalculate_combination(position)

        return Money.new(sets) if current_limit.zero?

        position = new_position

        return unless position

        adjust_to_new_position(position)
      end
    end

    private

    attr_reader :numbers, :values, :limits, :n
    attr_accessor :current_limit

    def recalculate_combination(position)
      position.upto n do |i|
        numbers[i] =
          max_number(values[i], combination_numbers[i], current_limit)

        limits[i] = current_limit

        rcalculate_current_limit(i)
      end
    end

    def rcalculate_current_limit(position)
      self.current_limit = current_limit - numbers[position] * values[position]
    end

    def new_position
      (n - 1).downto(0).detect { |i| combination_numbers[i].positive? }
    end

    def combination_numbers
      @combination_numbers ||= max_numbers.dup
    end

    def max_numbers
      @max_numbers ||=
        money.coin_sets.map do |set|
          max_number(set.value, set.number, limit)
        end
    end

    def max_number(value, number, limit)
      [limit / value, number].min
    end

    def sets
      values
        .zip(numbers)
        .map { |(value, number)| CoinSet.new(value: value, number: number) }
    end

    def adjust_to_new_position(position)
      combination_numbers[position] -= 1
      (position + 1).upto(n) { |i| combination_numbers[i] = max_numbers[i] }

      self.current_limit = limits[position]
    end
  end
end
