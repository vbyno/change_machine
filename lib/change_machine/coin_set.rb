# frozen_string_literal: true

class ChangeMachine
  # A set of similar coins
  class CoinSet
    attr_reader :value, :number

    def initialize(value:, number:)
      @value = value
      @number = number
    end

    def -(other)
      validate_subtraction!(other)

      self.class.new(number: number - other.number, value: value)
    end

    def <=>(other)
      -1 * (value <=> other.value)
    end

    def sum
      value * number
    end

    def to_a
      Array.new(number, value)
    end

    def ==(other)
      number == other.number && value == other.value
    end

    private

    def validate_subtraction!(other)
      if other.value != value
        raise ArgumentError, "Can't substract coins of different denomination"
      end

      return if other.number <= number

      raise ArgumentError, "Can't substract #{other.number} from #{number}"
    end
  end
end
